const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

const provider = new HDWalletProvider(
    '-Mneumonic-',
    '-Node-'    
);

const web3 = new Web3(provider);
let accounts;

const deploy = async () => {
    accounts = await web3.eth.getAccounts();
    
    console.log('Arrempting to deploy from accounts', accounts[0]);

    const result = await new web3.eth.Contract(JSON.parse(interface))
        .deploy({ data: bytecode, arguments: ['hey there!']})
        .send({gas: '1000000', from: accounts[0]});
    
    console.log('Contract deployed to', result.options.address);
}

deploy();